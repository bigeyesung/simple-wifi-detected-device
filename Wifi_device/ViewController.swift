
import Foundation
import UIKit
import SystemConfiguration

class ViewController: UIViewController
{

    @IBOutlet weak var circleLabel: UILabel!
    @IBOutlet weak var displayLabel: UILabel!
    @IBOutlet weak var WIFIButton: UIButton!
    
    @IBAction func TurnonWIFI(_ sender: Any) {
        
        let alertController = UIAlertController(title: "We need WIFI", message: "Go to Settings?", preferredStyle: .alert)
        var settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
            
            if let url = settingsUrl {
                UIApplication.shared.openURL(url as URL)
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
        
    
    }

    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.circleLabel.text = ""
        self.circleLabel.layer.masksToBounds = true
        self.circleLabel.layer.cornerRadius = self.circleLabel.frame.width/2
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.reachabilityStatusChanged), name: NSNotification.Name(rawValue: "ReachStatusChanged"), object: nil)
        self.reachabilityStatusChanged()
    }
    
    
    func reachabilityStatusChanged()
    {
        if reachabilityStatus == KNOTREACHABLE
        {
           self.displayLabel.text = "Offline"
           //self.view.backgroundColor = UIColor.red
           self.circleLabel.backgroundColor = UIColor.red
           WIFIButton.isEnabled = true
        }
        else if reachabilityStatus ==  KREACHABLEWITHWIFI
        {
           self.displayLabel.text = "Online"
           //self.view.backgroundColor = UIColor.green
           self.circleLabel.backgroundColor = UIColor.green
           WIFIButton.isEnabled = false
            
        }
        else if reachabilityStatus == KREACHABLEWITHWWAN
        {
           self.displayLabel.text = "Reachable via WWAN"
           self.view.backgroundColor = UIColor.yellow
        }
        
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReachStatusChanged"), object: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        
        
        
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    

}

